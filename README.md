# sxdv

Simple X Document Viewer.

## Dependencies

* libxcb
* mupdf

## Installation

* $ make
* # make install

## Key Bindings

* q - quit
* h - scroll to the left
* l - scroll to the right
* j - scroll down
* k - scroll up
* H - go to the top of the page
* L - go to the bottom of the page
* J - go to the next page
* K - go to the previous page
* + - zoom in
* - - zoom out
* = - reset zoom
* r - rotate the page clockwise
* R - rotate the page counterclockwise
* i - invert the page color

## Examples

Open a document:

```
$ sxdv document.pdf
```

