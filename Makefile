CFLAGS += -Wall -Wextra -pedantic -lxcb -lxcb-image -lmupdf

PREFIX ?= /usr/local
CC ?= cc

all: sxdv

sxdv: sxdv.c
	$(CC) sxdv.c $(CFLAGS) -o sxdv

install: sxdv
	install -Dm755 sxdv -t $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sxdv

clean:
	rm -f sxdv

.PHONY: all install uninstall clean

