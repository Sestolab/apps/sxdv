#include <stdio.h>
#include <stdlib.h>
#include <xcb/xcb.h>
#include <xcb/xcb_image.h>
#include <mupdf/fitz.h>


#define WINDOW_WIDTH 600
#define WINDOW_HEIGHT 800
#define MAX_ZOOM 300
#define SCROLL_STEP 50
#define DEFAULT_ZOOM 100
#define ZOOM_STEP 10


typedef struct view_t
{
	fz_context *ctx;
	fz_document *doc;
	fz_pixmap *pix;
	float zoom;
	int width;
	int height;
	int page_count;
	int page_number;
	int rotation;
	int scroll_x;
	int scroll_y;
	short int invert;
	short int redraw;
} view_t;


void
pixmap_from_page(view_t *view)
{
	fz_drop_pixmap(view->ctx, view->pix);

	fz_matrix ctm;
	ctm = fz_scale(view->zoom / 72, view->zoom / 72);
	ctm = fz_pre_rotate(ctm, view->rotation);

	fz_try(view->ctx)
		view->pix = fz_new_pixmap_from_page_number(view->ctx, view->doc, view->page_number, ctm,
			fz_device_bgr(view->ctx), 0);
	fz_catch(view->ctx)
	{
		fprintf(stderr, "Cannot render page: %s\n", fz_caught_message(view->ctx));
		return;
	}

	if (view->invert)
		fz_invert_pixmap(view->ctx, view->pix);
}

void
rotate_pixmap(view_t *view, int direction)
{
	view->rotation += direction;
	view->rotation = view->rotation % 360;

	fz_pixmap *new_pix = fz_new_pixmap(view->ctx, view->pix->colorspace, view->pix->h, view->pix->w, NULL, 0);

	if (direction < 0) for (int y = 0; y < view->pix->h; ++y) // based on raylib's ImageRotateCCW()
	{
		for (int x = 0; x < view->pix->w; ++x)
		{
			for (int i = 0; i < view->pix->n; ++i)
				new_pix->samples[(x * view->pix->h + y) * view->pix->n + i] =
					view->pix->samples[(y * view->pix->w + (view->pix->w - x - 1)) * view->pix->n + i];
		}
	}
	else for (int y = 0; y < view->pix->h; ++y) // based on raylib's ImageRotateCW()
	{
		for (int x = 0; x < view->pix->w; ++x)
		{
			for (int i = 0; i < view->pix->n; ++i)
				new_pix->samples[(x * view->pix->h + (view->pix->h - y - 1)) * view->pix->n + i] =
					view->pix->samples[(y * view->pix->w + x) * view->pix->n + i];
		}
	}

	fz_drop_pixmap(view->ctx, view->pix);
	view->pix = new_pix;
}


int
main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("Usage %s <file>\n", argv[0]);
		return 1;
	}

	view_t view = (view_t)
	{
		.ctx = NULL,
		.doc = NULL,
		.pix = NULL,
		.zoom = DEFAULT_ZOOM,
		.width = WINDOW_WIDTH,
		.height = WINDOW_HEIGHT,
		.page_count = 0,
		.page_number = 0,
		.rotation = 0,
		.scroll_x = 0,
		.scroll_y = 0,
		.invert = 0,
		.redraw = 0,
	};

	xcb_connection_t *c = xcb_connect(NULL, NULL);

	if (xcb_connection_has_error(c))
	{
		fprintf(stderr, "Could not connect to the X server.\n");
		xcb_disconnect(c);
		return 1;
	}

	view.ctx = fz_new_context(NULL, NULL, FZ_STORE_UNLIMITED);

	if (!view.ctx)
	{
		fprintf(stderr, "Cannot create mupdf context\n");
		xcb_disconnect(c);
		return 1;
	}

	fz_try(view.ctx) fz_register_document_handlers(view.ctx);
	fz_catch(view.ctx)
	{
		fprintf(stderr, "Cannot register document handlers: %s\n", fz_caught_message(view.ctx));
		fz_drop_context(view.ctx);
		xcb_disconnect(c);
		return 1;
	}

	fz_try(view.ctx) view.doc = fz_open_document(view.ctx, argv[1]);
	fz_catch(view.ctx)
	{
		fprintf(stderr, "Cannot open document: %s\n", fz_caught_message(view.ctx));
		fz_drop_context(view.ctx);
		xcb_disconnect(c);
		return 1;
	}

	fz_try(view.ctx) view.page_count = fz_count_pages(view.ctx, view.doc);
	fz_catch(view.ctx)
	{
		fprintf(stderr, "Cannot count number of pages: %s\n", fz_caught_message(view.ctx));
		fz_drop_document(view.ctx, view.doc);
		fz_drop_context(view.ctx);
		xcb_disconnect(c);
		return 1;
	}

	xcb_screen_t *s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;
	xcb_drawable_t w = xcb_generate_id(c);

	xcb_create_window(c, XCB_COPY_FROM_PARENT, w, s->root, 0, 0, view.width, view.height, 0,
		XCB_WINDOW_CLASS_INPUT_OUTPUT, s->root_visual, XCB_CW_BACK_PIXEL|XCB_CW_EVENT_MASK, (uint32_t[]){
		0x002b36, XCB_EVENT_MASK_EXPOSURE|XCB_EVENT_MASK_KEY_PRESS});

	xcb_gcontext_t gc = xcb_generate_id(c);;
	xcb_create_gc(c, gc, w, XCB_NONE, NULL);

	xcb_map_window(c, w);
	xcb_flush(c);

	short int should_close = 0;

	pixmap_from_page(&view);

	while (!should_close)
	{
		xcb_generic_event_t *e = xcb_wait_for_event(c);

		if (e == NULL) break;

		switch (e->response_type & ~0x80)
		{
			case XCB_EXPOSE:
			{
				view.redraw = 1;
				xcb_get_geometry_reply_t *g = xcb_get_geometry_reply(c, xcb_get_geometry(c, w), NULL);
				view.width = g->width;
				view.height = g->height;
				view.scroll_y = view.scroll_x = 0;
				free(g);
				break;
			}
			case XCB_KEY_PRESS:
			{
				xcb_key_press_event_t *keypress = (xcb_key_press_event_t *)e;
				switch (keypress->detail)
				{
					case 24: // [q] - quit
						should_close = 1;
						break;
					case 43: // [h] - scroll to the left; [H] - go to the top of the page
						if (!view.pix) break;
						if (view.pix->h > view.height && keypress->state == XCB_MOD_MASK_SHIFT)
						{
							view.scroll_y = 0;
							view.redraw = 1;
						}
						else if (view.pix->w > view.width)
						{
							if ((view.scroll_x -= SCROLL_STEP) < 0)
								view.scroll_x = 0;
							view.redraw = 1;
						}
						break;
					case 46: // [l] - scroll to the right; [L] - go to the bottom of the page
						if (!view.pix) break;
						if (view.pix->h > view.height && keypress->state == XCB_MOD_MASK_SHIFT)
						{
							view.scroll_y = view.height - view.pix->h;
							view.redraw = 1;
						}
						else if (view.pix->w > view.width)
						{
							if ((view.scroll_x += SCROLL_STEP) > view.pix->w- view.width)
								view.scroll_x = view.pix->w- view.width;
							view.redraw = 1;
						}
						break;
					case 44: // [j] - scroll down; [J] - go to the next page
						if (keypress->state == XCB_MOD_MASK_SHIFT)
						{
							if (++view.page_number > view.page_count - 1)
							{
								view.page_number = view.page_count - 1;
								break;
							}
							pixmap_from_page(&view);
							view.redraw = 1;
							view.scroll_y = view.scroll_x = 0;
						}
						else if (view.pix && view.pix->h > view.height)
						{
							if ((view.scroll_y += SCROLL_STEP) > view.pix->h- view.height)
								view.scroll_y = view.pix->h - view.height;
							view.redraw = 1;
						}
						break;
					case 45: // [k] - scroll up; [K] - go to the previous page
						if (keypress->state == XCB_MOD_MASK_SHIFT)
						{
							if (--view.page_number < 0)
							{
								view.page_number = 0;
								break;
							}
							pixmap_from_page(&view);
							view.redraw = 1;
							view.scroll_y = view.scroll_x = 0;
						}
						else if (view.pix && view.pix->h > view.height)
						{
							if ((view.scroll_y -= SCROLL_STEP) < 0)
								view.scroll_y = 0;
							view.redraw = 1;
						}
						break;
					case 21: // [=] - reset zoom; [+] - zoom in
						if (keypress->state == XCB_MOD_MASK_SHIFT)
						{
							if (view.zoom > MAX_ZOOM) break;
							else view.zoom += ZOOM_STEP;
						}
						else
						{
							view.zoom = DEFAULT_ZOOM;
							view.scroll_y = view.scroll_x = 0;
						}
						pixmap_from_page(&view);
						view.redraw = 1;
						break;
					case 20: // [-] - zoom out
						if (!view.pix || view.zoom == ZOOM_STEP) break;
						view.zoom -= ZOOM_STEP;
						pixmap_from_page(&view);
						if (view.height + view.scroll_y > view.pix->h &&
							(view.scroll_y -= view.height + view.scroll_y - view.pix->h) < 0) view.scroll_y = 0;
						if (view.width + view.scroll_x > view.pix->w &&
							(view.scroll_x -= view.width + view.scroll_x - view.pix->w) < 0) view.scroll_x = 0;
						view.redraw = 1;
						break;
					case 27: // [r] - rotate the page clockwise; [R] - rotate the page counterclockwise
						if (!view.pix) break;
						rotate_pixmap(&view, keypress->state == XCB_MOD_MASK_SHIFT ? -90 : 90);
						view.scroll_y = view.scroll_x = 0;
						view.redraw = 1;
						break;
					case 31: // [i] - invert the page color
						if (!view.pix) break;
						view.invert = !view.invert;
						fz_invert_pixmap(view.ctx, view.pix);
						view.redraw = 1;
						break;
					default: break;
				}
				break;
			}
			default: break;
		}
		free(e);

		if (view.redraw && view.pix)
		{
			xcb_image_t *img = xcb_image_create_native(c,
				view.pix->w > view.width ? view.width : view.pix->w,
				view.pix->h > view.height ? view.height : view.pix->h,
				XCB_IMAGE_FORMAT_Z_PIXMAP, s->root_depth, NULL, 0, NULL);

			uint32_t img_data_size = -1;
			for (int y = 0; y < img->height; ++y)
			{
				unsigned char *p = view.pix->samples + (view.scroll_y + y) * view.pix->stride +
					(view.scroll_x * view.pix->n);
				for (int x = 0; x < img->width; ++x)
				{
					img->data[++img_data_size] = p[0];
					img->data[++img_data_size] = p[1];
					img->data[++img_data_size] = p[2];
					img->data[++img_data_size] = 0;
					p += view.pix->n;
				}
			}

			int x = (view.width - img->width) / 2;
			int y = (view.height - img->height) / 2;

			if (view.pix->w < view.width)
			{
				xcb_clear_area(c, 0, w, 0, 0, x, view.height);
				xcb_clear_area(c, 0, w, img->width + x, 0, 0, view.height);
			}
			if (view.pix->h < view.height)
			{
				xcb_clear_area(c, 0, w, 0, 0, view.width, y);
				xcb_clear_area(c, 0, w, 0, img->height + y, view.width, 0);
			}

			xcb_image_put(c, w, gc, img, x, y, 0);
			xcb_image_destroy(img);
			xcb_flush(c);
			view.redraw = 0;
		}
	}

	fz_drop_pixmap(view.ctx, view.pix);
	fz_drop_document(view.ctx, view.doc);
	fz_drop_context(view.ctx);
	xcb_free_gc(c, gc);
	xcb_destroy_window(c, w);
	xcb_disconnect(c);
	return 0;
}

